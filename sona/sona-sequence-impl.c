#include "sona-sequence-private.h"

void sona_sequence_on_set_length (SonaSequence *sequence, guint length)
{
	sequence->_priv->length = length;
}

void sona_sequence_on_set_step (SonaSequence *sequence, guint pos, SonaStep *step)
{
	SonaStep *old = SONA_STEP (g_list_nth_data (sequence->_priv->steps, pos));
	if (old != NULL && old != step)
	{
		g_debug ("removing step %d", pos);
		sequence->_priv->steps = g_list_remove (sequence->_priv->steps, old);
		g_object_unref (old);

	}

	if ( step != NULL && old != step){
		g_debug ("setting step %d", pos);
		sequence->_priv->steps = g_list_insert (sequence->_priv->steps, step, pos);
		g_object_ref (step);
	}

}
