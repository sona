#include <stdio.h>
#include "sona-sequence.h"
#include "sona-player.h"

int main ()
{
	g_type_init ();
	SonaSequence* seq = sona_sequence_new (12);
	SonaStep *step = sona_step_new ();
	sona_sequence_set_step (seq, 0, step);
	sona_sequence_set_step (seq,  0, NULL);
	sona_sequence_set_step (seq, 0, step);
	sona_sequence_set_step (seq, 1, step);
	sona_sequence_set_step (seq, 2, step);
	g_debug ("%d", sona_sequence_get_n_steps (seq));
	SonaPlayer *player = sona_player_new (120);
	sona_player_play_sequence (player, seq, FALSE);
	return 0;
}
